## intent:utter.greeting
- hi
- hello
- good morning
- good afternoon
- good evening
- hay
- what's up
- yo
- hello what's up
- hi
- hey
- how are you doing
- hi
- what's up
- Yo
- are you ok?
- Hiya

## intent:utter.thankyou
- thankyou
- thanks
- so thanks
- thanks so much
- thank you so much
- thanksful
- thanks
- thank you 
- Thank you
- Thanks.
- Thanks a lot.
- Thank you very much.
- Thank you. That’s very kind of you.
- Thank you. You’re so helpful.
- Thanks for your kind words.
- Thank you for coming here today.

## intent:utter.accept
- confirm
- yes
- ok
- yes sure
- accept
- yes 
- OK
- like
- yeah
- It's okay
- I'll accept
- accept
- ok that right
- ok.
- Yes.
- yep

## intent:utter.decline
- no
- decline
- I don't
- do not
- don't
- don't like
- not like
- no no
- not
- no
- not ok
- no
- cancel

## intent:utter.notsure
- Not sure
- I'm not sure
- not sure
- I’m not sure.
- Please recheck again
- Please re-check again
- Please check it 
- Please verify data again
- I don't know it
- I don't know zipcode
- I do not know
- I don't know.
- I am not sure about zipcode
- not sure about destinaition zipcode
- not sure
- I'm not sure.
- I am not sure.

