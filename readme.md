train nlu
    - update data/AIYAIntentTemplate.xlsx
    - run "python bot.py train-nlu"

train-dialogue
    - update story in data/story.md
    - update domain.yml
    - run "python bot.py train-dialogue"

